import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjusteCalificacionesComponent } from './ajuste-calificaciones.component';

describe('AjusteCalificacionesComponent', () => {
  let component: AjusteCalificacionesComponent;
  let fixture: ComponentFixture<AjusteCalificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjusteCalificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjusteCalificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
