import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarPdComponent } from './generar-pd.component';

describe('GenerarPdComponent', () => {
  let component: GenerarPdComponent;
  let fixture: ComponentFixture<GenerarPdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarPdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarPdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
