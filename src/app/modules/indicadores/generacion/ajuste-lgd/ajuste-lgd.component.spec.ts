import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjusteLgdComponent } from './ajuste-lgd.component';

describe('AjusteLgdComponent', () => {
  let component: AjusteLgdComponent;
  let fixture: ComponentFixture<AjusteLgdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjusteLgdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjusteLgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
