import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColectivaComponent } from './colectiva.component';

describe('ColectivaComponent', () => {
  let component: ColectivaComponent;
  let fixture: ComponentFixture<ColectivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColectivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColectivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
