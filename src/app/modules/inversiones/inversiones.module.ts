import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

//  COMPONENTS

import { OperacionesComponent } from './insumos/operaciones/operaciones.component';
import { CdsComponent } from './insumos/cds/cds.component';
import { CurvaPdComponent } from './insumos/curva-pd/curva-pd.component';
import { GestionComponent } from './insumos/gestion/gestion.component';
import { StageComponent } from './reglas/stage/stage.component';
import { CalculoComponent } from './Reglas/calculo/calculo.component';
import { VistaPreviaComponent } from './resultados/vista-previa/vista-previa.component';
import { ReportesComponent } from './Resultados/reportes/reportes.component';

//  ROUTES

const routes: Routes = [
{
    path: 'inversiones',
    children: [
        {
            path: '',
            component: VistaPreviaComponent
        },
        { 
            path: 'insumos',      
            children: [
                { 
                    path: 'operaciones',      
                    component: OperacionesComponent 
                },
                { 
                    path: 'cds',      
                    component: CdsComponent 
                },
                { 
                    path: 'curva-pd',      
                    component: CurvaPdComponent 
                }
            ], 
        },
        { 
            path: 'reglas',      
            children: [
                { 
                    path: 'stage',      
                    component: StageComponent 
                },
                { 
                    path: 'calculo',      
                    component: CalculoComponent 
                }
            ], 
        },
        { 
            path: 'resultados',      
            children: [
                { 
                    path: 'vista-previa',      
                    component: VistaPreviaComponent 
                },
                { 
                    path: 'reportes',      
                    component: ReportesComponent 
                }
            ], 
        }
    ]
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    declarations: [
        OperacionesComponent, 
        CdsComponent, 
        CurvaPdComponent, 
        GestionComponent, 
        StageComponent, 
        CalculoComponent, 
        VistaPreviaComponent, 
        ReportesComponent]
})
export class InversionesModule { }
