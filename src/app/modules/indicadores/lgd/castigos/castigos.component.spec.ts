import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CastigosComponent } from './castigos.component';

describe('CastigosComponent', () => {
  let component: CastigosComponent;
  let fixture: ComponentFixture<CastigosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CastigosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CastigosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
