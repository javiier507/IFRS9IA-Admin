import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopiarPdComponent } from './copiar-pd.component';

describe('CopiarPdComponent', () => {
  let component: CopiarPdComponent;
  let fixture: ComponentFixture<CopiarPdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopiarPdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopiarPdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
