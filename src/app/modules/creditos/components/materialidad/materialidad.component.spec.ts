import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialidadComponent } from './materialidad.component';

describe('MaterialidadComponent', () => {
  let component: MaterialidadComponent;
  let fixture: ComponentFixture<MaterialidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
