import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

//  COMPONENTS GESTION
import { CarterasComponent } from './gestion/carteras/carteras.component';
import { InsumosComponent } from './gestion/insumos/insumos.component';
import { GarantiasComponent } from './gestion/garantias/garantias.component';
//  COMPONENTS PD
import { CalificacionesComponent } from './pd/calificaciones/calificaciones.component';
import { AjusteCalificacionesComponent } from './pd/ajuste-calificaciones/ajuste-calificaciones.component';
import { GenerarPdComponent } from './pd/generar-pd/generar-pd.component';
import { CopiarPdComponent } from './pd/copiar-pd/copiar-pd.component';
import { ReportesMensualesComponent } from './pd/reportes-mensuales/reportes-mensuales.component';
//  COMPONENTS LGD
import { IndividualComponent } from './lgd/individual/individual.component';
import { VolatilidadComponent } from './lgd/volatilidad/volatilidad.component';
import { SimulacionMontecarloComponent } from './lgd/simulacion-montecarlo/simulacion-montecarlo.component';
import { ColectivaComponent } from './lgd/colectiva/colectiva.component';
import { CastigosComponent } from './lgd/castigos/castigos.component';
//  COMPONENTS GENERACION
import { AjusteLgdComponent } from './generacion/ajuste-lgd/ajuste-lgd.component';
import { AplicacionComponent } from './Generacion/aplicacion/aplicacion.component';

//  ROUTES

const routes: Routes = [
{
    path: 'indicadores',
    children: [
        {
            path: '',
            component: CarterasComponent
        },
        { 
            path: 'gestion',      
            children: [
                { 
                    path: 'carteras',      
                    component: CarterasComponent 
                },
                { 
                    path: 'insumos',      
                    component: InsumosComponent 
                },
                { 
                    path: 'garantias',      
                    component: GarantiasComponent 
                }
            ], 
        },
        { 
            path: 'pd',      
            children: [
                { 
                    path: 'calificaciones',      
                    component: CalificacionesComponent 
                },
                { 
                    path: 'ajuste',      
                    component: AjusteCalificacionesComponent 
                },
                { 
                    path: 'generacion',      
                    component: GenerarPdComponent 
                },
                { 
                    path: 'copiar',      
                    component: CopiarPdComponent 
                },
                { 
                    path: 'reportes-mensuales',      
                    component: ReportesMensualesComponent 
                }
            ], 
        },
        { 
            path: 'lgd',      
            children: [
                { 
                    path: 'individual',      
                    component: IndividualComponent 
                },
                { 
                    path: 'volatilidad',      
                    component: VolatilidadComponent 
                },
                { 
                    path: 'simulacion-montecarlo',      
                    component: SimulacionMontecarloComponent 
                },
                { 
                    path: 'colectiva',      
                    component: ColectivaComponent 
                },
                { 
                    path: 'castigos',      
                    component: CastigosComponent 
                }
            ], 
        },
        { 
            path: 'generacion',      
            children: [
                { 
                    path: 'ajuste-lgd',      
                    component: AjusteLgdComponent 
                },
                { 
                    path: 'aplicacion',      
                    component: AplicacionComponent 
                }
            ], 
        },
    ]
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    declarations: [
        CarterasComponent, 
        InsumosComponent, 
        GarantiasComponent, 
        CalificacionesComponent, 
        AjusteCalificacionesComponent, 
        GenerarPdComponent, 
        CopiarPdComponent, 
        ReportesMensualesComponent, 
        IndividualComponent, 
        VolatilidadComponent, 
        SimulacionMontecarloComponent, 
        ColectivaComponent, 
        CastigosComponent, 
        AjusteLgdComponent, 
        AplicacionComponent]
})
export class IndicadoresModule { }
