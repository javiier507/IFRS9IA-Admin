import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurvaPdComponent } from './curva-pd.component';

describe('CurvaPdComponent', () => {
  let component: CurvaPdComponent;
  let fixture: ComponentFixture<CurvaPdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurvaPdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurvaPdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
