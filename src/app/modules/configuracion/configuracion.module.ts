import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

//  COMPONENTS

import { UsuariosComponent } from './seguridad/usuarios/usuarios.component';
import { BitacorasComponent } from './seguridad/bitacoras/bitacoras.component';
import { BaseDatosComponent } from './base-datos/base-datos.component';
import { DominiosComponent } from './dominios/dominios.component';

//  ROUTES

const routes: Routes = [
{
    path: 'configuracion',
    children: [
        {
            path: '',
            component: BitacorasComponent
        },
        { 
            path: 'seguridad',      
            children: [
                { 
                    path: 'usuarios',      
                    component: UsuariosComponent 
                },
                { 
                    path: 'bitacora',      
                    component: BitacorasComponent 
                }
            ], 
        },
        { 
            path: 'base-datos',
            component: BaseDatosComponent
        },
        { 
            path: 'dominios', 
            component: DominiosComponent
        }
    ]
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    declarations: [
        UsuariosComponent, 
        BitacorasComponent, 
        BaseDatosComponent, 
        DominiosComponent
    ]
})
export class ConfiguracionModule { }
