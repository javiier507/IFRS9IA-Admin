import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'inicio', title: 'Inicio',  icon: 'pe-7s-graph', class: '' },
    { path: 'indicadores', title: 'Indicadores Créditos',  icon:'pe-7s-user', class: '' },
    { path: 'creditos', title: 'Créditos',  icon:'pe-7s-note2', class: '' },
    { path: 'inversiones', title: 'Inversiones',  icon:'pe-7s-news-paper', class: '' },
    { path: 'cuentas', title: 'Cuentas Por Cobrar',  icon:'pe-7s-science', class: '' },
    { path: 'configuracion', title: 'Configuracion',  icon:'pe-7s-bell', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
