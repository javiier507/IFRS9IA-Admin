import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesMensualesComponent } from './reportes-mensuales.component';

describe('ReportesMensualesComponent', () => {
  let component: ReportesMensualesComponent;
  let fixture: ComponentFixture<ReportesMensualesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesMensualesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesMensualesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
