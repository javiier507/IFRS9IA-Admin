import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

//  COMPONENTS

import { StageComponent } from './components/stage/stage.component';
import { PdComponent } from './components/pd/pd.component';
import { LgdComponent } from './components/lgd/lgd.component';
import { EadComponent } from './components/ead/ead.component';
import { CcfComponent } from './components/ccf/ccf.component';
import { VencimientoComponent } from './components/vencimiento/vencimiento.component';
import { FlComponent } from './components/fl/fl.component';
import { MaterialidadComponent } from './components/materialidad/materialidad.component';
import { VistaPreviaComponent } from './components/vista-previa/vista-previa.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { EscalasComponent } from './components/lgd/escalas/escalas.component';

//  ROUTES

const routes: Routes =[
    {
        path: 'creditos',
        children: [
            { 
                path: '',      
                component: VistaPreviaComponent 
            },
            { 
                path: 'vista-previa',      
                component: VistaPreviaComponent 
            },
            { 
                path: 'stage',      
                component: StageComponent 
            },
            { 
                path: 'pd',           
                component: PdComponent 
            },
            { 
                path: 'lgd',          
                children: [
                    {
                        path: '',
                        component: LgdComponent
                    },
                    {
                        path: 'escalas',
                        component: EscalasComponent
                    }
                ]
            },
            { 
                path: 'ead',          
                component: EadComponent 
            },
            { 
                path: 'ccf',          
                component: CcfComponent 
            },
            { 
                path: 'vencimiento',          
                component: VencimientoComponent 
            },
            { 
                path: 'fl',          
                component: FlComponent 
            },
            { 
                path: 'materialidad',          
                component: MaterialidadComponent 
            },
            { 
                path: 'reportes',          
                component: ReportesComponent 
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    declarations: [
        StageComponent, 
        PdComponent, 
        LgdComponent, 
        EadComponent, 
        CcfComponent, 
        VencimientoComponent, 
        FlComponent, 
        MaterialidadComponent, 
        VistaPreviaComponent, 
        ReportesComponent, 
        EscalasComponent
    ]
})
export class CreditosModule { }
