import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulacionMontecarloComponent } from './simulacion-montecarlo.component';

describe('SimulacionMontecarloComponent', () => {
  let component: SimulacionMontecarloComponent;
  let fixture: ComponentFixture<SimulacionMontecarloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulacionMontecarloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulacionMontecarloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
