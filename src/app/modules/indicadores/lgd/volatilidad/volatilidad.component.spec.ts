import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolatilidadComponent } from './volatilidad.component';

describe('VolatilidadComponent', () => {
  let component: VolatilidadComponent;
  let fixture: ComponentFixture<VolatilidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolatilidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolatilidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
