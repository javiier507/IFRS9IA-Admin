import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

//  MODULES DEFUALT

import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './components/shared/navbar/navbar.module';
import { FooterModule } from './components/shared/footer/footer.module';
import { SidebarModule } from './components/shared/sidebar/sidebar.module';
import { LbdModule } from './components/shared/lbd/lbd.module';

//  MODULES IFRS9

import { IndicadoresModule } from './modules/indicadores/indicadores.module';
import { CreditosModule } from './modules/creditos/creditos.module';
import { InversionesModule } from './modules/inversiones/inversiones.module';
import { ConfiguracionModule } from './modules/configuracion/configuracion.module';

//  COMPONENTS

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';

//  CONFIG

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        NavbarModule,
        FooterModule,
        SidebarModule,
        RouterModule,
        AppRoutingModule,
        LbdModule,
        IndicadoresModule,
        CreditosModule,
        InversionesModule,
        ConfiguracionModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
