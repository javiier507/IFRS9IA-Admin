import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcfComponent } from './ccf.component';

describe('CcfComponent', () => {
  let component: CcfComponent;
  let fixture: ComponentFixture<CcfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
